const {
  PackagesGetList,
  PackagesGet,
  PackagesAdd,
  PackagesEdit,
  PackagesDelete,
  PackagesOrderPackages,
  PackagesGetByPricingId,
  PackagesGetAll,
  PackagesSearch,
  PackagesGetSearchCount,
} = require("./models/packages");
const { UsersAuth, UsersLogin } = require("./models/users");
const { PackageGroupsGetAll } = require("./models/packageGroups");
const {
  PricingsGet,
  PricingsGetAll,
  PricingsGetList,
  PricingsGetListCount,
} = require("./models/pricings");
const { ModuleManagerGetAll } = require("./models/moduleManager");

/**
 * Create an instance of a BlestaApi!
 * @param {String} apiUrl URL to API (https://mybelstasite.com/api/)
 * @param {String} apiUser Name for
 */

class BlestaApi {
  constructor(apiUrl, apiUser, apiKey) {
    this.apiKey = apiKey;
    this.apiUser = apiUser;
    this.apiUrl = apiUrl;
  }

  //  PACKAGES MODEL

  Packages = {
    add: async ({ vars }) =>
      await PackagesAdd(this.apiUrl, this.apiUser, this.apiKey, vars),
    edit: async ({ package_id, vars }) =>
      await PackagesEdit(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        package_id,
        vars
      ),
    delete: async ({ package_id }) =>
      await PackagesDelete(this.apiUrl, this.apiUser, this.apiKey, package_id),
    getByPricingId: async ({ package_pricing_id }) =>
      await PackagesGetByPricingId(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        package_pricing_id
      ),
    getAll: async ({ company_id, order, status, type }) =>
      await PackagesGetAll(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id,
        order,
        status,
        type
      ),
    orderPackages: async ({ package_group_id, package_ids }) =>
      await PackagesOrderPackages(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        package_group_id,
        package_ids
      ),
    /**
     *  Get a paginated list of packages
     * @param {integer} companyId Company ID
     * @param {integer} page Page Number for Pagination
     * @param {string} status Filter by package status (accepts "active", "inactive" and "restricted")
     * @returns {object} Parsed JSON result from Blesta
     */
    getList: async ({ company_id, page, status }) =>
      await PackagesGetList(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id,
        page,
        status
      ),
    search: async ({ query, page }) =>
      await PackagesSearch(this.apiUrl, this.apiUser, this.apiKey, query, page),
    /**
     *  Get a specific package by ID
     * @param {integer} packageId
     * @returns {object} Parsed JSON result from Blesta
     */
    getSearchCount: async ({ query, page }) =>
      await PackagesGetSearchCount(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        query
      ),
    /**
     *  Get a specific package by ID
     * @param {integer} packageId
     * @returns {object} Parsed JSON result from Blesta
     */
    get: async ({ package_id }) =>
      await PackagesGet(this.apiUrl, this.apiUser, this.apiKey, package_id),
  };

  // PACKAGEGROUPS MODEL
  PackageGroups = {
    getAll: async ({ company_id, type }) =>
      await PackageGroupsGetAll(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id,
        type
      ),
  };

  Pricings = {
    get: async ({ pricing_id }) =>
      await PricingsGet(this.apiUrl, this.apiUser, this.apiKey, pricing_id),
    getAll: async ({ company_id }) =>
      await PricingsGetAll(this.apiUrl, this.apiUser, this.apiKey, company_id),
    getList: async ({ company_id, page, order_by }) =>
      await PricingsGetList(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id,
        page,
        order_by
      ),
    getListCount: async ({ company_id }) =>
      await PricingsGetListCount(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id
      ),
  };

  // MODULEMANAGER MODEL

  ModuleManager = {
    getAll: async ({ company_id, sort_by, order }) =>
      await ModuleManagerGetAll(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        company_id,
        sort_by,
        order
      ),
  };

  //  USERS MODEL

  Users = {
    /**
     *  Get a paginated list of packages
     * @param {string} username Username or UID
     * @param {object} vars Expects an object with {username: 'string', password: 'string'}
     * @param {string} type The type of user to authenticate. Accepts "any", "staff", "client" or "contact".
     * @returns {boolean} True if the user can be authenticated, false otherwise
     */
    auth: async ({ username, vars, type }) =>
      await UsersAuth(
        this.apiUrl,
        this.apiUser,
        this.apiKey,
        username,
        vars,
        type
      ),
    /**
     *  Login a user to a session
     *
     *  http://source-docs.blesta.com/class-Users.html#_login
     *
     *  __This function requires a session token from PHP, and cannot be used as a result. Included for completeness only__
     *
     *  ```
     *  Users.login({
     *    session: Session,
     *    vars: {
     *      username: String,
     *      password: String
     *      otp: String,
     *      ip_address: String,
     *      remember_me: Boolean
     *    }
     * })
     * ```
     *  @param {object} _ Object
     */
    login: async ({ session, vars }) =>
      await UsersLogin(this.apiUrl, this.apiUser, this.apiKey, session, vars),
  };
}

module.exports = BlestaApi;
