const axios = require("axios");

async function PricingsGet(aUrl, aUser, aKey, pricing_id) {
  const params = new URLSearchParams();
  params.append("pricing_id", pricing_id);
  const r = await axios({
    method: "get",
    url: aUrl + "/pricings/get.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(r.data.message);
  }
}

async function PricingsGetAll(aUrl, aUser, aKey, company_id) {
  const params = new URLSearchParams();
  params.append("company_id", company_id);
  const r = await axios({
    method: "get",
    url: aUrl + "/pricings/getAll.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(r.data.message);
  }
}

async function PricingsGetList(aUrl, aUser, aKey, cId, page, order_by) {
  if (!cId) {
    return Promise.reject("Function expects company_id");
  }
  const params = new URLSearchParams({
    company_id: cId,
  });
  if (page) {
    params.append("page", page);
  }
  if (order_by) {
    params.append(
      `order_by[${Object.keys(order_by)[0]}]`,
      order_by[Object.keys(order_by)[0]]
    );
  }
  const r = await axios({
    method: "get",
    url: aUrl + "pricings/getlist.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return [];
  });

  return r.data.response;
}

async function PricingsGetListCount(aUrl, aUser, aKey, company_id) {
  if (!company_id) {
    throw new Error("Function expects Number company_id");
  }
  const params = new URLSearchParams();
  params.append("company_id", company_id);

  const r = await axios({
    method: "get",
    url: aUrl + "/pricings/getlistcount.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  return r.data.response;
}

module.exports.PricingsGet = PricingsGet;
module.exports.PricingsGetAll = PricingsGetAll;
module.exports.PricingsGetList = PricingsGetList;
module.exports.PricingsGetListCount = PricingsGetListCount;
