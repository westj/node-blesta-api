const axios = require("axios");
const url = require("url");

async function UsersAuth(aUrl, aUser, aKey, username, vars, type) {
  if (!username || !vars.username || !vars.password) {
    if ([username, vars.username, vars.password].includes(undefined)) {
      throw new Error(
        "Function expects " +
          ["username", "vars.username", "vars.password"][
            [username, vars.username, vars.password].indexOf(undefined)
          ]
      );
    }
  }
  const params = new url.URLSearchParams({
    username,
    ["vars[username]"]: vars.username,
    ["vars[password]"]: vars.password,
  });
  if (type) {
    params.append("type", type);
  }

  const r = await axios({
    method: "post",
    url: aUrl + "users/auth.json",
    auth: {
      username: aUser,
      password: aKey,
    },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: params.toString(),
  }).catch((e) => {
    console.log(e);
    return [];
  });

  return r.data.response;
}

async function UsersLogin(aUrl, aUser, aKey, session, vars) {
  if (!session || !vars.username || !vars.password) {
    if ([session, vars.username, vars.password].includes(undefined)) {
      throw new Error(
        "Function expects " +
          ["session", "vars.username", "vars.password"][
            [session, vars.username, vars.password].indexOf(undefined)
          ]
      );
    }
  }
  const params = new url.URLSearchParams({
    session,
    ["vars[username]"]: vars.username,
    ["vars[password]"]: vars.password,
  });
  if (vars.ip_address) {
    params.append("vars[ip_address]", vars.ip_address);
  }
  if (vars.otp) {
    params.append("vars[otp]", vars.otp);
  }
  if (vars.remember_me) {
    params.append("vars[remember_me]", vars.remember_me);
  }

  const r = await axios({
    method: "post",
    url: aUrl + "users/login.json",
    auth: {
      username: aUser,
      password: aKey,
    },
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    data: params.toString(),
  }).catch((e) => {
    return Promise.reject(
      "Server responded with " +
        e.response.status +
        ": " +
        e.response.statusText +
        ". " +
        e.response.data.response
    );
  });
  return r.data.response;
}

exports.UsersAuth = UsersAuth;
exports.UsersLogin = UsersLogin;
