const axios = require("axios");

async function ModuleManagerGetAll(
  aUrl,
  aUser,
  aKey,
  company_id,
  sort_by,
  order
) {
  if (!company_id) {
    throw new Error("Function expects number company_id");
  }
  const params = new URLSearchParams();
  params.append("company_id", company_id);
  sort_by ? params.append("sort_by", sort_by) : null;

  if (order) {
    params.append(
      `order[${Object.keys(order)[0]}]`,
      order[Object.keys(order)[0]]
    );
  }

  const r = await axios({
    method: "get",
    url: aUrl + "moduleManager/getAll.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e.message);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(
      JSON.stringify(r.message) +
        ` ${
          r.response
            ? " | Blesta error " + JSON.stringify(r.response.data.message)
            : ""
        }`
    );
  }
}

exports.ModuleManagerGetAll = ModuleManagerGetAll;
