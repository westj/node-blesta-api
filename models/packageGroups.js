const axios = require("axios");

async function PackageGroupsGetAll(aUrl, aUser, aKey, company_id, type) {
  if (!company_id) {
    throw new Error("Function expects number company_id");
  }
  const params = new URLSearchParams();
  params.append("company_id", company_id);
  type ? params.append("type", type) : null;

  const r = await axios({
    method: "get",
    url: aUrl + "packageGroups/getAll.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e.message);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(
      JSON.stringify(r.message) +
        ` ${
          r.response
            ? " | Blesta error " + JSON.stringify(r.response.data.message)
            : ""
        }`
    );
  }
}

exports.PackageGroupsGetAll = PackageGroupsGetAll;
