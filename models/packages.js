const axios = require("axios");

// add
async function PackagesAdd(aUrl, aUser, aKey, vars) {
  if (!vars.names || !vars.company_id || !vars.email_content || !vars.status) {
    if (
      [vars.names, vars.company_id, vars.email_content, vars.status].includes(
        undefined
      )
    ) {
      return Promise.reject(
        new Error(
          "Function expects " +
            [
              "vars.names",
              "vars.company_id",
              "vars.email_content",
              "vars.status",
            ][
              [
                vars.names,
                vars.company_id,
                vars.email_content,
                vars.status,
              ].indexOf(undefined)
            ]
        )
      );
    }
  }

  let params = new URLSearchParams();

  let shallow = [
    "module_id",
    "name",
    "description",
    "qty",
    "module_row",
    "module_group",
    "taxable",
    "single_term",
    "company_id",
    "prorata_day",
    "prorata_cutoff",
  ];

  Object.keys(vars).forEach((el) => {
    if (shallow.includes(el)) {
      params.append(`vars[${el}]`, `${vars[el]}`);
    }
    if (el == "names") {
      try {
        vars.names.forEach((ell, index) => {
          if (!ell.name) {
            throw new Error(`Missing vars.names[${index}].name`);
          } else if (!ell.lang) {
            throw new Error(`Missing vars.names[${index}].name`);
          } else {
            params.append(`vars[names][${index}][name]`, ell.name);
            params.append(`vars[names][${index}][lang]`, ell.lang);
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "descriptions") {
      try {
        vars.descriptions.forEach((ell, index) => {
          if (!ell.lang) {
            throw new Error(`Missing vars.descriptions[${index}].lang`);
          } else {
            ell.html
              ? params.append(
                  `vars[descriptions][${index}][html]`,
                  ell.html || ""
                )
              : null;
            ell.text
              ? params.append(
                  `vars[descriptions][${index}][text]`,
                  ell.text || ""
                )
              : null;
            ell.lang
              ? params.append(`vars[descriptions][${index}][lang]`, ell.lang)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "email_content") {
      try {
        vars.email_content.forEach((ell, index) => {
          if (!ell.lang) {
            throw new Error(`Missing vars.email_content[${index}].lang`);
          } else {
            ell.html
              ? params.append(
                  `vars[email_content][${index}][html]`,
                  ell.html || ""
                )
              : null;
            ell.text
              ? params.append(
                  `vars[email_content][${index}][text]`,
                  ell.text || ""
                )
              : null;
            ell.lang
              ? params.append(`vars[email_content][${index}][lang]`, ell.lang)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "pricing") {
      try {
        vars.pricing.forEach((ell, index) => {
          if (!ell.currency) {
            throw new Error(`Missing vars.pricing[${index}].currency`);
          } else {
            params.append(
              `vars[pricing][${index}][currency]`,
              ell.currency || ""
            );
            ell.term
              ? params.append(`vars[pricing][${index}][term]`, ell.term)
              : null;
            ell.period
              ? params.append(`vars[pricing][${index}][period]`, ell.period)
              : null;
            ell.price
              ? params.append(`vars[pricing][${index}][price]`, ell.price)
              : null;
            ell.setup_fee
              ? params.append(
                  `vars[pricing][${index}][setup_fee]`,
                  ell.setup_fee
                )
              : null;
            ell.cancel_fee
              ? params.append(
                  `vars[pricing][${index}][cancel_fee]`,
                  ell.cancel_fee
                )
              : null;
            ell.currency
              ? params.append(`vars[pricing][${index}][currency]`, ell.currency)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "meta") {
      try {
        vars.meta.forEach((ell) => {
          if (typeof ell == "Array") {
            ell.forEach((elll, index) => {
              params.append(`vars[meta][${ell}][${index}][${elll}]`, elll);
            });
          } else {
            params.append(`vars[meta][${ell}]`, ell);
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "groups") {
      if (vars.groups[0]) {
        vars[el].forEach((ell, index) => {
          params.append(`vars[groups][${index}]`, ell);
        });
      } else {
        throw new Error("vars.groups expects an array");
      }
    }
    if (el == "option_groups") {
      if (typeof vars[el] == "Array") {
        vars[el].forEach((ell, index) => {
          params.append(`vars[option_groups][${index}][${ell}]`, ell);
        });
      } else {
        throw new Error("vars.option_groups expects an array");
      }
    }
    if (el == "status") {
      if (
        !vars[el].includes("active") &&
        !vars[el].includes("inactive") &&
        !vars[el].includes("restricted")
      ) {
        throw new Error(
          "Invalid status. Use 'restricted', 'active' or 'inactive"
        );
      } else {
        params.append("vars[status]", vars[el]);
      }
    }
  });
  const r = await axios({
    method: "get",
    url: aUrl + "/packages/add.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (r.isAxiosError) {
    console.log(r.response.data.errors);
    throw new Error(r.response.data.message);
  } else {
    return r.data.response;
  }
}
// edit
async function PackagesEdit(aUrl, aUser, aKey, package_id, vars) {
  if (
    !package_id ||
    !vars.names ||
    !vars.company_id ||
    !vars.email_content ||
    !vars.status
  ) {
    if (
      [
        package_id,
        vars.names,
        vars.company_id,
        vars.email_content,
        vars.status,
      ].includes(undefined)
    ) {
      return Promise.reject(
        new Error(
          "Function expects " +
            [
              "package_id",
              "vars.names",
              "vars.company_id",
              "vars.email_content",
              "vars.status",
            ][
              [
                package_id,
                vars.names,
                vars.company_id,
                vars.email_content,
                vars.status,
              ].indexOf(undefined)
            ]
        )
      );
    }
  }

  let params = new URLSearchParams();

  params.append("package_id", package_id);

  let shallow = [
    "module_id",
    "name",
    "description",
    "qty",
    "module_row",
    "module_group",
    "taxable",
    "single_term",
    "company_id",
    "prorata_day",
    "prorata_cutoff",
  ];

  Object.keys(vars).forEach((el) => {
    if (shallow.includes(el)) {
      params.append(`vars[${el}]`, `${vars[el]}`);
    }
    if (el == "names") {
      try {
        vars.names.forEach((ell, index) => {
          if (!ell.name) {
            throw new Error(`Missing vars.names[${index}].name`);
          } else if (!ell.lang) {
            throw new Error(`Missing vars.names[${index}].name`);
          } else {
            params.append(`vars[names][${index}][name]`, ell.name);
            params.append(`vars[names][${index}][lang]`, ell.lang);
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "descriptions") {
      try {
        vars.descriptions.forEach((ell, index) => {
          if (!ell.lang) {
            throw new Error(`Missing vars.descriptions[${index}].lang`);
          } else {
            ell.html
              ? params.append(
                  `vars[descriptions][${index}][html]`,
                  ell.html || ""
                )
              : null;
            ell.text
              ? params.append(
                  `vars[descriptions][${index}][text]`,
                  ell.text || ""
                )
              : null;
            ell.lang
              ? params.append(`vars[descriptions][${index}][lang]`, ell.lang)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "email_content") {
      try {
        vars.email_content.forEach((ell, index) => {
          if (!ell.lang) {
            throw new Error(`Missing vars.email_content[${index}].lang`);
          } else {
            ell.html
              ? params.append(
                  `vars[email_content][${index}][html]`,
                  ell.html || ""
                )
              : null;
            ell.text
              ? params.append(
                  `vars[email_content][${index}][text]`,
                  ell.text || ""
                )
              : null;
            ell.lang
              ? params.append(`vars[email_content][${index}][lang]`, ell.lang)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "pricing") {
      try {
        vars.pricing.forEach((ell, index) => {
          if (!ell.currency) {
            throw new Error(`Missing vars.pricing[${index}].currency`);
          } else {
            params.append(
              `vars[pricing][${index}][currency]`,
              ell.currency || ""
            );
            ell.term
              ? params.append(`vars[pricing][${index}][term]`, ell.term)
              : null;
            ell.period
              ? params.append(`vars[pricing][${index}][period]`, ell.period)
              : null;
            ell.price
              ? params.append(`vars[pricing][${index}][price]`, ell.price)
              : null;
            ell.setup_fee
              ? params.append(
                  `vars[pricing][${index}][setup_fee]`,
                  ell.setup_fee
                )
              : null;
            ell.cancel_fee
              ? params.append(
                  `vars[pricing][${index}][cancel_fee]`,
                  ell.cancel_fee
                )
              : null;
            ell.currency
              ? params.append(`vars[pricing][${index}][currency]`, ell.currency)
              : null;
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "meta") {
      try {
        vars.meta.forEach((ell) => {
          if (typeof ell == "Array") {
            ell.forEach((elll, index) => {
              params.append(`vars[meta][${ell}][${index}][${elll}]`, elll);
            });
          } else {
            params.append(`vars[meta][${ell}]`, ell);
          }
        });
      } catch (e) {
        throw new Error(e);
      }
    }
    if (el == "groups") {
      if (vars.groups[0]) {
        vars[el].forEach((ell, index) => {
          params.append(`vars[groups][${index}]`, ell);
        });
      } else {
        throw new Error("vars.groups expects an array");
      }
    }
    if (el == "option_groups") {
      if (typeof vars[el] == "Array") {
        vars[el].forEach((ell, index) => {
          params.append(`vars[option_groups][${index}][${ell}]`, ell);
        });
      } else {
        throw new Error("vars.option_groups expects an array");
      }
    }
    if (el == "status") {
      if (
        !vars[el].includes("active") &&
        !vars[el].includes("inactive") &&
        !vars[el].includes("restricted")
      ) {
        throw new Error(
          "Invalid status. Use 'restricted', 'active' or 'inactive"
        );
      } else {
        params.append("vars[status]", vars[el]);
      }
    }
  });
  const r = await axios({
    method: "get",
    url: aUrl + "packages/edit.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return true;
  } else {
    throw new Error(r.data.message);
  }
}
// delete
async function PackagesDelete(aUrl, aUser, aKey, package_id) {
  const params = new URLSearchParams({ package_id });
  const r = await axios({
    method: "get",
    url: aUrl + "packages/delete.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return true;
  } else {
    throw new Error(r.data.message);
  }
}
// orderPackages
async function PackagesOrderPackages(
  aUrl,
  aUser,
  aKey,
  package_group_id,
  package_ids
) {
  if (!package_group_id) {
    throw new Error("Function expects number package_group_id");
  } else if (!package_ids[0]) {
    throw new Error("Function expects array package_id");
  }
  const params = new URLSearchParams();
  params.append("package_group_id", package_group_id);
  package_ids.forEach((el, index) => {
    params.append(`package_ids[${index}]`, el);
  });

  const r = await axios({
    method: "get",
    url: aUrl + "packages/orderPackages.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return true;
  } else {
    throw new Error(r.data.message);
  }
}
// get
async function PackagesGet(aUrl, aUser, aKey, pId) {
  if (!pId) {
    return Promise.reject(new Error("Function expects package_id"));
  }
  const params = new URLSearchParams({
    package_id: pId,
  });
  const r = await axios({
    method: "get",
    url: aUrl + "packages/get.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return {};
  });

  return r.data.response;
}
// getByPricingId
async function PackagesGetByPricingId(aUrl, aUser, aKey, package_pricing_id) {
  if (!package_pricing_id) {
    throw new Error("Function expects number package_pricing_id");
  }
  const params = new URLSearchParams();
  params.append("package_pricing_id", package_pricing_id);
  const r = await axios({
    method: "get",
    url: aUrl + "packages/getByPricingId.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(r.response.data.message);
  }
}
// getAll
async function PackagesGetAll(
  aUrl,
  aUser,
  aKey,
  company_id,
  order,
  status,
  type
) {
  if (!company_id) {
    throw new Error("Function expects number company_id");
  }
  const params = new URLSearchParams();
  params.append("company_id", company_id);
  status ? params.append("status", status) : null;
  type ? params.append("type", type) : null;

  if (order) {
    params.append(
      `order[${Object.keys(order)[0]}]`,
      order[Object.keys(order)[0]]
    );
  }

  const r = await axios({
    method: "get",
    url: aUrl + "packages/getAll.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e.message);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(
      JSON.stringify(r.message) +
        ` ${
          r.response
            ? " | Blesta error " + JSON.stringify(r.response.data.message)
            : ""
        }`
    );
  }
}
// getList
async function PackagesGetList(aUrl, aUser, aKey, cId, page, status) {
  if (!cId) {
    return Promise.reject("Function expects company_id");
  }
  const params = new URLSearchParams({
    company_id: cId,
  });
  if (page) {
    params.append("page", page);
  }
  if (status) {
    params.append("status", status);
  }

  const r = await axios({
    method: "get",
    url: aUrl + "packages/getlist.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return [];
  });

  return r.data.response;
}
// search
async function PackagesSearch(aUrl, aUser, aKey, query, page) {
  if (!query) {
    throw new Error("Function expects string query");
  }

  let params = new URLSearchParams();
  params.append("query", query);
  page ? params.append("page", page) : null;

  const r = await axios({
    method: "get",
    url: aUrl + "packages/search.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(r.data.message);
  }
}
// getSearchCount
async function PackagesGetSearchCount(aUrl, aUser, aKey, query) {
  if (!query) {
    throw new Error("Function expects string query");
  }

  let params = new URLSearchParams();
  params.append("query", query);

  const r = await axios({
    method: "get",
    url: aUrl + "packages/getsearchcount.json",
    params,
    auth: {
      username: aUser,
      password: aKey,
    },
  }).catch((e) => {
    console.log(e);
    return e;
  });

  if (!r.isAxiosError) {
    return r.data.response;
  } else {
    throw new Error(r.data.message);
  }
}

exports.PackagesGet = PackagesGet;
exports.PackagesGetList = PackagesGetList;
exports.PackagesAdd = PackagesAdd;
exports.PackagesEdit = PackagesEdit;
exports.PackagesDelete = PackagesDelete;
exports.PackagesOrderPackages = PackagesOrderPackages;
exports.PackagesGetByPricingId = PackagesGetByPricingId;
exports.PackagesGetAll = PackagesGetAll;
exports.PackagesSearch = PackagesSearch;
exports.PackagesGetSearchCount = PackagesGetSearchCount;
